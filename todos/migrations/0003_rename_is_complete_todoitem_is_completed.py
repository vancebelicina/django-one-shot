# Generated by Django 4.2.1 on 2023-05-31 23:10

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("todos", "0002_todoitem"),
    ]

    operations = [
        migrations.RenameField(
            model_name="todoitem",
            old_name="is_complete",
            new_name="is_completed",
        ),
    ]
